package jpp.tcrush.gamelogic;

import java.util.*;
import jpp.tcrush.gamelogic.field.GameFieldElement;
import jpp.tcrush.gamelogic.field.GameFieldElementType;
import jpp.tcrush.gamelogic.utils.Coordinate2D;
import jpp.tcrush.gamelogic.utils.Move;

public class Level {
    Boolean aBoolean;
    private Map <Coordinate2D, GameFieldElement> fieldMap;
    private Collection <Shape> allowedShapes;
    private int width = 0, high = 0;

    public Level(Map <Coordinate2D, GameFieldElement> fieldMap, Collection <Shape> allowedShapes) {
        if (fieldMap == null || allowedShapes == null || allowedShapes.isEmpty()) {
            throw new IllegalArgumentException();
        }

        for (Coordinate2D coordinaarde : fieldMap.keySet()) {
            if (coordinaarde.getX() > width) {
                width = coordinaarde.getX();
            }
            if (coordinaarde.getY() > high) {
                high = coordinaarde.getY();
            }
        }
        width = width + 1;
        high = high + 1;

        if (width < 2 || high < 2) {
            throw new IllegalArgumentException();
        }

        this.fieldMap = fieldMap;
        this.allowedShapes = allowedShapes;
    }

    public int getHeight() {

        return high;
    }

    public int getWidth() {
        return width;
    }

    public Map <Coordinate2D, GameFieldElement> getField() {
        return Collections.unmodifiableMap(fieldMap);
    }

    public Collection <Shape> getAllowedShapes() {
        return Collections.unmodifiableCollection(allowedShapes);
    }

    public Optional <Collection <Coordinate2D>> fit(Shape shape, Coordinate2D position) {
        if (shape == null || position == null) {
            throw new IllegalArgumentException();
        }
        Collection <Coordinate2D> coordinate2DCollection = shape.getPoints();
        Collection <Coordinate2D> coordinate2DS = new ArrayList <>();
        for (Coordinate2D coordinate2D3d : coordinate2DCollection) {
            Coordinate2D valueCoordinate = new Coordinate2D(position.getX() + coordinate2D3d.getX(), position.getY() - coordinate2D3d.getY());
            GameFieldElement gameFieldElement = this.getField().get(valueCoordinate);
            if (coordinate2DS.isEmpty()) {
                if (gameFieldElement == null || gameFieldElement.getType() != GameFieldElementType.CELL || !gameFieldElement.getItem().isPresent()) {
                    return Optional.empty();
                }
                coordinate2DS.add(valueCoordinate);
            } else {
                GameFieldElement gameFieldElementVerified = this.getField().get(coordinate2DS.iterator().next());

                if (gameFieldElement == null || gameFieldElement.getType() != GameFieldElementType.CELL || !gameFieldElement.getItem().isPresent() || gameFieldElement.getItem().get().getType() != gameFieldElementVerified.getItem().get().getType()) {
                    return Optional.empty();
                }

                coordinate2DS.add(valueCoordinate);

            }
        }

        return Optional.of(coordinate2DS);
    }

    public Collection <Move> setShapeAndUpdate(Shape shape, Coordinate2D position) {
        System.out.println("update");
        System.out.println("shape: "+shape);
            System.out.println("amount"+shape.getAmount());
            System.out.println("present "+this.fit(shape, position).isPresent());
            System.out.println("containt: "+allowedShapes.contains(shape));
        if (shape == null || position == null || shape.getAmount() <= 0 || !this.fit(shape, position).isPresent() || !allowedShapes.contains(shape)) {
            
            throw new IllegalArgumentException();
        }

        Collection <Coordinate2D> coordinate2DCollection = fit(shape, position).get();
        Collection <Move> moves = new ArrayList <>();
        for (Coordinate2D coordonateMove : coordinate2DCollection) {
            fieldMap.get(coordonateMove).setItem(Optional.empty());
        }

        for (Coordinate2D coordinateMove : coordinate2DCollection) {
            if (fieldMap.get(coordinateMove).getPredecessor().isPresent()) {
                if (fieldMap.get(coordinateMove).getPredecessor().get().getType().equals(GameFieldElementType.CELL)) {

                    fieldMap.get(coordinateMove).getPredecessor().get().update(moves);
                }

            }
        }

        Shape shape1 = null;
        for (Shape shapeMoves : allowedShapes) {
            if (shapeMoves.equals(shape)) {

                shapeMoves.reduceAmount();
                if (shapeMoves.getAmount() == 0) {
                    shape1 = shapeMoves;
                }
            }

        }
        if (shape1 != null) {

            allowedShapes.remove(shape1);
        }

        System.out.println(moves.toString());
        return moves;
    }

    public boolean isWon() {
        Iterator <GameFieldElement> gameFieldElements = fieldMap.values().iterator();

        while (gameFieldElements.hasNext()) {
            GameFieldElement gameFieldElement = gameFieldElements.next();
            if (gameFieldElement.getType() == GameFieldElementType.CELL && gameFieldElement.getItem().isPresent()) {
                return false;
            }
        }
        return true;
    }

    public boolean canMakeAnyMove() {
        Iterator <Coordinate2D> coordinate2DIterator = fieldMap.keySet().iterator();

        for (Shape shapeMoves:allowedShapes) {
            while (coordinate2DIterator.hasNext()) {
                Coordinate2D coordinate2D = coordinate2DIterator.next();
                if (fit(shapeMoves, coordinate2D).isPresent()){
                    return true;
                }
            }
        }
        return false;


    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("TCrush-LevelDefinition:\n");
        for (int j = 0; j < getHeight(); j++) {
            for (int i = 0; i < getWidth(); i++) {
                Coordinate2D coordinate2D = new Coordinate2D(i, j);
                GameFieldElement gameFieldElementtest = fieldMap.get(coordinate2D);
                stringBuilder.append(gameFieldElementtest);
            }
            stringBuilder.append("\n");
        }

        stringBuilder.append("\n");
        stringBuilder.append("Shapes:\n");
        for (Shape shape : allowedShapes) {

            stringBuilder.append(shape + "\n");
        }
        stringBuilder.deleteCharAt(stringBuilder.lastIndexOf("\n"));
        System.out.println(stringBuilder.toString());
        return stringBuilder.toString();
    }

    public static void main(String[] args) {

        Collection <Shape> shape;

        GameFieldElement gameFieldElement;
        Map <Coordinate2D, GameFieldElement> coordinate2DGameFieldElementMap = new HashMap <>();

    }

}
