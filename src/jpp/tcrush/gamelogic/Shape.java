package jpp.tcrush.gamelogic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import jpp.tcrush.gamelogic.utils.Coordinate2D;

public class Shape {

    private Collection <Coordinate2D> points;
    private String name;
    private int amount;
    public Shape(Collection <Coordinate2D> points, String name, int amount){
        if (points == null || name == null || amount <= 0 || points.isEmpty()){
            throw new IllegalArgumentException();
        }
        this.amount = amount;
        this.name = name;
        this.points = points;
    }

    public Shape(Collection<Coordinate2D> points, int amount){
        if (points == null || amount <= 0 || points.isEmpty()){
            throw new IllegalArgumentException();
        }
        this.points = points;
        this.amount = amount;
    }

    public int getAmount() {

        return amount;
    }

    public Collection <Coordinate2D> getPoints() {
        points = Collections.unmodifiableCollection(points);
        return points;
    }


    public boolean reduceAmount(){
        amount--;
        return amount > 0;
    }

    @Override
    public String toString() {

        String stringBuilder = amount + ":";
        if (name != null){
            return amount + ":" + name;
        }else {

            for (Coordinate2D coordinate: points) {
                stringBuilder += coordinate + ";";

            }
        }

        return stringBuilder;
    }

    public static Shape getPointShape(int amount){
        Collection<Coordinate2D> coordinate2DCollection = new ArrayList <>();
        coordinate2DCollection.add(new Coordinate2D(0,0));
        return new Shape(coordinate2DCollection,"point", amount );
    }

    public static Shape getsRowShape(int amount){
        Collection<Coordinate2D> coordinate2DCollection = new ArrayList <>();
        coordinate2DCollection.add(new Coordinate2D(0,0));
        coordinate2DCollection.add(new Coordinate2D(-1,0));

        return new Shape(coordinate2DCollection, "sRow", amount);
    }

    public static Shape getsColumnShape(int amount){
        Collection<Coordinate2D> coordinate2DCollection = new ArrayList <>();
        coordinate2DCollection.add(new Coordinate2D(0,0));
        coordinate2DCollection.add(new Coordinate2D(0, 1));
        return new Shape(coordinate2DCollection, "sColumn", amount);
    }

    public static Shape getRowShape(int amount){
        Collection<Coordinate2D> coordinate2DCollection = new ArrayList <>();
        coordinate2DCollection.add(new Coordinate2D(0,0));
        coordinate2DCollection.add(new Coordinate2D(1,0));
        coordinate2DCollection.add(new Coordinate2D(-1,0));
        return new Shape(coordinate2DCollection,"row", amount);
    }

    public static Shape getColumnShape(int amount){
        Collection<Coordinate2D> coordinate2DCollection = new ArrayList <>();
        coordinate2DCollection.add(new Coordinate2D(0,0));
        coordinate2DCollection.add(new Coordinate2D(0,1));
        coordinate2DCollection.add(new Coordinate2D(0, 2));
        return new Shape(coordinate2DCollection, "column", amount);
    }

    public static Shape getUpTShape(int amount){
        Collection<Coordinate2D> coordinate2DCollection = new ArrayList <>();
        coordinate2DCollection.add(new Coordinate2D(0,0));
        coordinate2DCollection.add(new Coordinate2D(0,1));
        coordinate2DCollection.add(new Coordinate2D(-1,0));
        coordinate2DCollection.add(new Coordinate2D(1,0));
        return new Shape(coordinate2DCollection, "upT", amount);
    }

    public static Shape getDownTShape(int amount){
        Collection<Coordinate2D> coordinate2DCollection = new ArrayList <>();
        coordinate2DCollection.add(new Coordinate2D(0,0));
        coordinate2DCollection.add(new Coordinate2D(0,1));
        coordinate2DCollection.add(new Coordinate2D(1,1));
        coordinate2DCollection.add(new Coordinate2D(-1,1));
        return new Shape(coordinate2DCollection, "downT", amount);
    }

    public static Shape getRightTShape(int amount){
        Collection<Coordinate2D> coordinate2DCollection = new ArrayList <>();
        coordinate2DCollection.add(new Coordinate2D(0,0));
        coordinate2DCollection.add(new Coordinate2D(0,1));
        coordinate2DCollection.add(new Coordinate2D(0,2));
        coordinate2DCollection.add(new Coordinate2D(1,1));
        return new Shape(coordinate2DCollection,"rightT", amount);
    }

    public static Shape getLeftTShape(int amount){
        Collection<Coordinate2D> coordinate2DCollection = new ArrayList <>();
        coordinate2DCollection.add(new Coordinate2D(0,0));
        coordinate2DCollection.add(new Coordinate2D(0,1));
        coordinate2DCollection.add(new Coordinate2D(0,2));
        coordinate2DCollection.add(new Coordinate2D(-1,1));
        return new Shape(coordinate2DCollection, "leftT", amount);
    }


    @Override
    public boolean equals(Object o) {


        if (this == o) return true;
        if (!(o instanceof Shape)) return false;

        Shape shape = (Shape) o;

        return  shape.getPoints().containsAll(this.getPoints()) && this.getPoints().containsAll(shape.getPoints());
        //points.equals(shape.points);
    }

    @Override
    public int hashCode() {
        return points.hashCode();
    }
}
