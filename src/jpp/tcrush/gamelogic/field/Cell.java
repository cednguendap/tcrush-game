package jpp.tcrush.gamelogic.field;

import java.util.Collection;
import java.util.Optional;
import jpp.tcrush.gamelogic.utils.Coordinate2D;
import jpp.tcrush.gamelogic.utils.Move;

public class Cell implements GameFieldElement{

    private GameFieldItem item;
    private Coordinate2D pos;
    private GameFieldElement predecessor;
    private GameFieldElement successor;

    public Cell(GameFieldItem item, Coordinate2D pos){

        if (pos == null){
            throw new IllegalArgumentException();
        }
        this.item = item;
        this.pos = pos;
        
    }

    @Override
    public Optional <GameFieldItem> getItem() {

        return Optional.ofNullable(item);
    }

    @Override
    public GameFieldElementType getType() {

        return GameFieldElementType.CELL;
    }

    @Override
    public Optional <GameFieldElement> getPredecessor() {

        return Optional.ofNullable(this.predecessor);
    }

    @Override
    public Optional <GameFieldElement> getSuccessor() {

        return Optional.ofNullable(this.successor);
    }

    @Override
    public Coordinate2D getPos() {
        return pos;
    }

    @Override
    public String toString() {
        if (item == null){
            return "n";
        }else if (item.getType() == GameFieldItemType.BLUE) {
            return "b";
        }else if (item.getType() == GameFieldItemType.GREEN){
            return "g";
        }else if (item.getType() == GameFieldItemType.RED){
            return "r";
        }else if (item.getType() == GameFieldItemType.YELLOW){
            return "y";
        }else if (item.getType() == GameFieldItemType.PURPLE){
            return "p";
        }else if (item.getType() == GameFieldItemType.BLACK){
            return "B";
        }else if (item.getType() == GameFieldItemType.ORANGE){
            return "o";
        }else {
            return "";
        }
    }



    @Override
    public void setItem(Optional <GameFieldItem> item) {
        this.item = item.orElse(null);
    }

    @Override
    public void setPredecessor(GameFieldElement field) {
        if (field == null){
            throw new IllegalArgumentException();
        }

        if (field.getType() == GameFieldElementType.BLOCK){
            predecessor = null;
        }
        this.predecessor = field;
    }

    @Override
    public void setSuccessor(GameFieldElement field) {
        if (field == null){
            throw new IllegalArgumentException();
        }
        if (field.getType() == GameFieldElementType.BLOCK){
            successor = null;
        }
        this.successor = field;
    }

    @Override
    public void update(Collection <Move> moves) {

        if (this.item == null){
            return;
        }
        if (getSuccessor().isPresent() && getSuccessor().get().getItem().isEmpty()){

            getSuccessor().get().setItem(this.getItem());
            this.setItem(Optional.empty());                     //         zurücksetzung des items.
            if (!this.getSuccessor().get().getItem().get().isOnMove()){
                this.getSuccessor().get().getItem().get().startMove(this.getPos());
            }
            this.successor.update(moves);

        }else if (!this.getSuccessor().isPresent() || this.getItem().isPresent()){
            if (this.getItem().get().isOnMove()){

                moves.add(this.getItem().get().endMove(this.getPos()));
            }
        }

        if (this.getPredecessor().isPresent()){
            this.predecessor.update(moves);
        }

    }
}
