package jpp.tcrush.gamelogic.field;

import java.util.Collection;
import java.util.Optional;
import jpp.tcrush.gamelogic.utils.Coordinate2D;
import jpp.tcrush.gamelogic.utils.Move;

public interface GameFieldElement {

    public Optional <GameFieldItem> getItem();

    public GameFieldElementType getType();

    public Optional<GameFieldElement> getPredecessor();

    public Optional<GameFieldElement> getSuccessor();

    public Coordinate2D getPos();

    public void setItem(Optional<GameFieldItem> item);

    public void setPredecessor(GameFieldElement field);

    public void setSuccessor(GameFieldElement field);

    public void update(Collection <Move> moves);

    public static GameFieldElement createCell(GameFieldItem item, Coordinate2D pos){

        if (pos == null){
            throw new IllegalArgumentException();
        }

        return new Cell(item, pos);
    }

    public static GameFieldElement createBlock(Coordinate2D pos){

        if (pos == null){
            throw new IllegalArgumentException();
        }

        return new Block(pos);
    }

    public static GameFieldElement createFallthrough(Coordinate2D pos){

        if (pos == null){
            throw new IllegalArgumentException();
        }

        return new Fallthrough(pos);
    }


}
