package jpp.tcrush.gamelogic.field;

import jpp.tcrush.gamelogic.utils.Coordinate2D;
import jpp.tcrush.gamelogic.utils.Move;

public class GameFieldItem {

    private GameFieldItemType type;
    private Coordinate2D startPosition;
    private Coordinate2D endPosition;
    private boolean isStarted;

    public GameFieldItem(GameFieldItemType type) {
        this.type = type;
    }
    
    public GameFieldItemType getType() {
        return type;
    }

    public void startMove(Coordinate2D startPosition) {
        if (startPosition == null) {
            throw new IllegalArgumentException();
        }
        if (isStarted) {
            throw new IllegalStateException();
        }
        this.startPosition = startPosition;
        isStarted = true;
    }

    public Move endMove(Coordinate2D endPosition) {
        if (endPosition == null) {
            throw new IllegalArgumentException();
        }
        if (!isStarted) {
            throw new IllegalStateException();
        }
        this.endPosition = endPosition;
        Move move = new Move(startPosition, endPosition);
        isStarted = false;
        return move;
    }

    public boolean isOnMove() {
       return isStarted;
    }
}
