/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpp.tcrush.gui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import jpp.tcrush.gamelogic.Level;
import jpp.tcrush.gamelogic.Shape;
import jpp.tcrush.gamelogic.field.GameFieldElement;
import jpp.tcrush.gamelogic.field.GameFieldElementType;
import jpp.tcrush.gamelogic.field.GameFieldItem;
import jpp.tcrush.gamelogic.field.GameFieldItemType;
import jpp.tcrush.gamelogic.utils.Coordinate2D;
import jpp.tcrush.gamelogic.utils.Move;
import jpp.tcrush.gui.utils.Pair;

/**
 *
 * @author ppitbull
 */
public class Game {
    private Level level;
    private GridPane gameDashboard;
    private HBox shapeDashboard;
    private int nc=0;
    private Pair<GridPane, Shape> currentSelectedShape=null;
    private Node[][] gridNode;
        
    public Game(Level level, GridPane dashboard, HBox shapeD)
    {
        this.level=level;
        this.gameDashboard=dashboard;
        this.shapeDashboard=shapeD;        
    }
    public Game()
    {}
    
    public void init()
    {
        clear();
        this.initDashboard();
        this.initShape();
    }
    public void clear()
    {
       gameDashboard.getChildren().clear();
       shapeDashboard.getChildren().clear();
       currentSelectedShape=null;
       gridNode=null;
    }
    public void initDashboard()
    {  
        gridNode=new Node[level.getWidth()][level.getHeight()];
        
        
        for(Map.Entry<Coordinate2D, GameFieldElement> element:level.getField().entrySet())
        {
            Node node=TCrushGui.drawElement(element.getValue().getType());
            
            if(element.getValue().getType()==GameFieldElementType.CELL)
            {
                GameFieldItemType  type=element.getValue().getItem().get().getType();
                if(type==GameFieldItemType.RED)
                {
                    ((Circle)node.lookup("#cercle")).setFill(Color.RED);
                }
                else if(type==GameFieldItemType.BLACK)
                {
                    ((Circle)node.lookup("#cercle")).setFill(Color.BLACK);
                }
                else if(type==GameFieldItemType.BLUE)
                {
                    ((Circle)node.lookup("#cercle")).setFill(Color.BLUE);
                }
                else if(type==GameFieldItemType.GREEN)
                {
                    ((Circle)node.lookup("#cercle")).setFill(Color.GREEN);
                }
                else if(type==GameFieldItemType.ORANGE)
                {
                    ((Circle)node.lookup("#cercle")).setFill(Color.ORANGE);
                }
                else if(type==GameFieldItemType.PURPLE)
                {
                    ((Circle)node.lookup("#cercle")).setFill(Color.PURPLE);
                }
                else if(type==GameFieldItemType.YELLOW)
                {
                    ((Circle)node.lookup("#cercle")).setFill(Color.YELLOW);
                }
            }
            addEvent(node,element.getKey());
            gridNode[element.getKey().getX()][element.getKey().getY()]=node;     
            gameDashboard.add(node,element.getKey().getX(),element.getKey().getY());
        }
        //updateUI();        
    }
    
    public void initShape()
    {
        Collection<Shape> shapes=level.getAllowedShapes();
        for(Shape shape:shapes)
        {
            StackPane anchorShape=new StackPane();            
            anchorShape.setAlignment(Pos.CENTER);
            //ici on traite l'affichage de la grille des éléments
            GridPane gridShape=new GridPane();
            
            gridShape.setHgap(3);
            gridShape.setVgap(3);
            gridShape.setAlignment(Pos.CENTER);
            gridShape.getStyleClass().add("all_rep_shape");
            //gridShape.setNodeOrientation(NodeOrientation.);
            gridShape.setOnMouseClicked((MouseEvent event)->  {
                //if(event) doit verifier si c'est un clique gauche avant d'appeler la methode approprié
                    handleSelectShapeEvent(shape,gridShape);
            });
            int neg=0;
            for(Coordinate2D point:shape.getPoints())
            {
                point.setY(-1*point.getY());
                
                if(point.getX()<neg) neg=point.getX();
                if(point.getY()<neg) neg=point.getY();
               
            }   
            for(Coordinate2D point:shape.getPoints())
            {
                //System.out.println("Point "+point);
                try {
                    FXMLLoader loader=new FXMLLoader();
                    loader.setLocation(Game.class.getResource("./fxml/rep_block.fxml"));
                    gridShape.add(loader.load() ,point.getX()-neg,point.getY()-neg);
                } catch (IOException ex) {
                    Logger.getLogger(GameFieldItem.class.getName())
                            .log(java.util.logging.Level.SEVERE, null, ex);
                }                
            }   
            //ici on traite l'affichage du text
            Label labShape=new Label("x"+shape.getAmount());
            labShape.setAlignment(Pos.CENTER);
            labShape.setTextFill(Color.web("rgba(0,0,0,0.9)"));
            
            anchorShape.getChildren().add(gridShape);
            anchorShape.getChildren().add(labShape);
                     
            //on affiche le block
            shapeDashboard.getChildren().add(anchorShape);
        }
    }
    
    public void addEvent(Node node,Coordinate2D coord)
    {
        node.setOnMouseEntered((event)->handleMouseDragInGridShape(coord, node));
        node.setOnMouseExited((event)->handleMouseDragOutGridShape(coord, node));
        node.setOnMouseClicked((event)->handleMouseClickGridShape(coord, node));
    }
    public void updateUI()
    {
       gameDashboard.getChildren().clear();
       shapeDashboard.getChildren().clear();
       currentSelectedShape=null;
        for(int i=0;i<level.getWidth();i++)
        {
            for(int j=0;j<level.getHeight();j++)
            {
                gameDashboard.add(gridNode[i][j],i,j);
                addEvent(gridNode[i][j],new Coordinate2D(i,j));
            }
        }
        initShape();
    }

    public void handleSelectShapeEvent(Shape shape,GridPane gridShape)
    {
        nc++;
       System.out.println("Clicked "+nc);
       if(currentSelectedShape!=null)
       {
           currentSelectedShape.getFirst().getStyleClass().remove("rep_shape");
       }
       gridShape.getStyleClass().add("rep_shape");
       currentSelectedShape=new Pair<>(gridShape,shape);
    }
    public void handleMouseDragInGridShape(Coordinate2D coord,Node noeud)
    {
       if(currentSelectedShape!=null)
       {
            for(Coordinate2D point:currentSelectedShape.getSecond().getPoints())
            {
                if( 
                        coord.getX()+point.getX()<level.getWidth() &&
                        coord.getX()+point.getX()>=0 &&
                        coord.getY()+point.getY()<level.getHeight() &&
                        coord.getY()+point.getY()>=0
                    )
                {
                    gridNode[coord.getX()+point.getX()][coord.getY()+point.getY()]
                            .getStyleClass().add("rep_shape");  
                }
            }            
       }
        
    }
    
   
    private void handleMouseDragOutGridShape(Coordinate2D coord, Node noeud) {
       if(currentSelectedShape!=null)
       {
            noeud.getStyleClass().remove("rep_shape");    
            
            for(Coordinate2D point:currentSelectedShape.getSecond().getPoints())
            {
                if(!point.equals(coord) && 
                        coord.getX()+point.getX()<level.getWidth() &&
                        coord.getX()+point.getX()>=0 &&
                        coord.getY()+point.getY()<level.getHeight() &&
                        coord.getY()+point.getY()>=0
                        )
                {
                    gridNode[coord.getX()+point.getX()][coord.getY()+point.getY()]
                            .getStyleClass().remove("rep_shape");  
                }
            }
       }
    }
    private void handleMouseClickGridShape(Coordinate2D point, Node node) {
        
        if(currentSelectedShape!=null)
       {
           //winLevelUI();
           //Coordinate2D center=ShapeFunction.getValidPosition(ShapeFunction.getShapeCenter(currentSelectedShape.getSecond()),currentSelectedShape.getSecond()); 
           Collection<Coordinate2D> coords=new ArrayList<>();
            currentSelectedShape.getSecond().getPoints().stream().forEach((Coordinate2D c)->{
                coords.add(new Coordinate2D(c.getX(),-1*c.getY()));
            });
           Shape shapeToMove=new Shape(coords,currentSelectedShape.getSecond().getAmount());
  
           Optional<Collection<Coordinate2D>> coll=level.fit(shapeToMove,point);
           System.out.println("fit "+coll+" "+coll.isEmpty());
           if(!coll.isEmpty())
           {
               Collection<Move> move=level.setShapeAndUpdate(shapeToMove, point);
               for(Move dep:move)
               {
                   Node noeud=gridNode[dep.getFrom().getX()][dep.getFrom().getY()];
                   gridNode[dep.getFrom().getX()][dep.getFrom().getY()]=TCrushGui.drawElement(GameFieldElementType.FALLTHROUGH);
                   gridNode[dep.getTo().getX()][dep.getTo().getY()]=noeud;
               }
               updateUI();
               if(level.isWon()) winLevelUI();
               if(!level.canMakeAnyMove()) loseLevelUI();
           }
       }
    }
    public void winLevelUI()
    {
        Dialog dialog=new Dialog();
        dialog.setTitle("Etat du jeux");
        dialog.setContentText("Bravo!!!! Vous avez terminé la partie.\nvoulez vous charger une nouvelle?");
        dialog.setOnCloseRequest((event)-> dialog.close());
        dialog.show();
    }
    public void loseLevelUI()
    {
        Dialog dialog=new Dialog();
        dialog.setTitle("Etat du jeux");
        dialog.setContentText("Vous avez perdu la partie!! \nvoulez vous chargez une nouvelle?");
        dialog.setOnCloseRequest((event)-> dialog.close());
        dialog.show();
    }
    
    public void setLevel(Level level) {
        this.level = level;
    }

    public void setGameDashboard(GridPane gameDashboard) {
        this.gameDashboard = gameDashboard;
    }

    public void setShapeDashboard(HBox shapeDashboard) {
        this.shapeDashboard = shapeDashboard;
    }
   
}
