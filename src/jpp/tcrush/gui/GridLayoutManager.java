package jpp.tcrush.gui;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import jpp.tcrush.gamelogic.Level;
import jpp.tcrush.gamelogic.Shape;
import jpp.tcrush.gamelogic.field.GameFieldElement;
import jpp.tcrush.gamelogic.utils.Coordinate2D;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.Map;


public class  GridLayoutManager extends JFrame {

    private Container contents;
    private Level level;
    private JButton [][] squares;

    //private  int row = level.getWidth()-1;
    private int col = 1;
    private Color colorBlack = Color.BLACK;
    private Cursor cursor = new Cursor(ICONIFIED);


    public GridLayoutManager(Level level){

        super("GUI TCrusch");
        this.level = level;
        contents = getContentPane();
        contents.setLayout(new GridLayout(level.getWidth(),level.getHeight()));
        squares = new JButton[level.getWidth()][level.getHeight()];
        Map <Coordinate2D, GameFieldElement> fieldMap = level.getField();
        Collection <Shape> allowedShapes = level.getAllowedShapes();

        ButtonHandler buttonHandler = new ButtonHandler();


        for (Map.Entry<Coordinate2D, GameFieldElement> entry : fieldMap.entrySet()) {
            Coordinate2D coordinate2D = entry.getKey();
            GameFieldElement gameFieldElement = entry.getValue();

            squares[coordinate2D.getX()][coordinate2D.getY()].setBackground(gamefiedItemTypeColor(gameFieldElement.getItem().get().toString()));
            contents.add(squares[coordinate2D.getX()][coordinate2D.getY()]);
            squares[coordinate2D.getX()][coordinate2D.getY()].addActionListener(buttonHandler);

        }

        for (Shape shapeLevel : level.getAllowedShapes()) {
            javafx.scene.canvas.Canvas canvas = new Canvas();
            GraphicsContext gc = canvas.getGraphicsContext2D();
            gc.strokeRect(0,0,10,10);
            gc.setFill(javafx.scene.paint.Color.RED);

        }

        //squares[row][col].setCursor(cursor);
        setSize(500,500);
        setResizable(false);
        setLocationRelativeTo(null);
        setVisible(true);

    }

    private class ButtonHandler implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            Object source = e.getSource();

            for (int i = 0; i < level.getWidth(); i++) {
                for (int j = 0; j < level.getHeight(); j++) {
                    if (source == squares[i][j]){
                        processClick();
                        return;
                    }
                }
            }
        }
    }

    private void processClick(){
        for (Map.Entry<Coordinate2D,GameFieldElement> elementEntry: level.getField().entrySet()) {
            GameFieldElement gameFieldElement = elementEntry.getValue();

            if (!gameFieldElement.getItem().get().isOnMove()) {
                return;
            }

           // squares[row][col].setIcon(null);
        }
    }


    public Color gamefiedItemTypeColor(String value) {
        switch (value) {
            case "B":
                return java.awt.Color.BLACK;
            case "b":
                return java.awt.Color.BLUE;
            case "r":
                return java.awt.Color.RED;
            case "p":
                return new Color(102,0,153);
            case "y":
                return java.awt.Color.YELLOW;
            case "o":
                return java.awt.Color.ORANGE;
            case "n":
                return java.awt.Color.WHITE;
            case "#":
                return Color.gray;
            case "+":
                return java.awt.Color.PINK;
            default:
                return Color.MAGENTA;
        }
    }
}
