package jpp.tcrush.gui;

import java.io.IOException;
import java.util.Map;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.*;
import javafx.scene.transform.Affine;
import javafx.stage.Stage;
import jpp.tcrush.gamelogic.Level;
import jpp.tcrush.gamelogic.Shape;
import jpp.tcrush.gamelogic.field.GameFieldElement;
import jpp.tcrush.gamelogic.field.GameFieldElementType;
import jpp.tcrush.gamelogic.utils.Coordinate2D;


public class TCrushGui extends Application {


    private static Level level;
    public static Map <Coordinate2D, GameFieldElement> fieldMap;
    //568
    //71
    
    public TcrushController tcrushController;
    public GridLayoutManager gridLayoutManager;
    private Shape shape;
    private Affine affine;
    private Canvas canvas;
    private GridPane gridPane;
    private Scene scene;
    ScrollPane scrollPane;
    private BorderPane borderPane;
    private StackPane stackPane;
    private Stage primaryStage;
    private SimpleStringProperty tCruschDefTest;
    private SimpleStringProperty levelGewinn;
    private SimpleStringProperty consoleText;

    public TCrushGui() {

        this.tCruschDefTest = new SimpleStringProperty("TCrush-LevelDefinition:");
        this.consoleText = new SimpleStringProperty("Level fehlschalgen! Kein Zug mehr möglich!"); // Level gelöst
        this.levelGewinn = new SimpleStringProperty("Level gelöst");

    }


    public void start(Stage primaryStage) throws IOException {

        primaryStage.setTitle("TCrush");
        this.tcrushController = new TcrushController(primaryStage);
        
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(TCrushGui.class.getResource("fxml/ui.fxml"));
        loader.setController(tcrushController);
        BorderPane root = (BorderPane)loader.load();
        
        Scene scene = new Scene(root);
        tcrushController.setContainer((GridPane)root.getCenter().lookup("#gamedashboard"));
        tcrushController.getGame().setShapeDashboard((HBox)root.getBottom().lookup("#shapezone"));
        primaryStage.setScene(scene);
        primaryStage.show();

    }




    private void submitStatusMessage(String text) {
        this.consoleText.set(this.consoleText.get() + "\n" + text);
    }

    private void submitErrorMessage(String text) {
        this.levelGewinn.set(this.levelGewinn.get() + "\n" + text);
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
    
   
    static Node drawElement(GameFieldElementType gameFieldElementType) {
        Node node=null;
       
        String name="";
        if(gameFieldElementType==GameFieldElementType.BLOCK) name="block.fxml";
        else if(gameFieldElementType==GameFieldElementType.CELL) name="cell.fxml";
        else name="fallthrought.fxml";
        
        try {
            FXMLLoader loader=new FXMLLoader();
            loader.setLocation(TCrushGui.class.getResource("fxml/"+name));
            node=(Pane)loader.load();
        } catch (IOException ex) {
            Logger.getLogger(TCrushGui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        return node;
    }
}
