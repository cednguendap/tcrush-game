package jpp.tcrush.gui;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.Map;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleMapProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;
import jpp.tcrush.gamelogic.Level;
import jpp.tcrush.gamelogic.Shape;
import jpp.tcrush.gamelogic.field.GameFieldElement;
import jpp.tcrush.gamelogic.utils.Coordinate2D;
import jpp.tcrush.parse.LevelParser;


public class TcrushController {

    private SimpleStringProperty gamefieldElement;
    LevelParser levelParser;
    private Level level;
    public GridPane gridPane;
    private Map <Coordinate2D, GameFieldElement> mapLevel = new LinkedHashMap <>();
    private Shape shapeLevel;


    private SimpleStringProperty coordinate2D;
    private SimpleStringProperty testBegin;
    private SimpleStringProperty tCruschDefTest;
    private SimpleStringProperty levelGewinn;
    private SimpleStringProperty consoleText;
    private SimpleStringProperty LevelParser;
    private SimpleObjectProperty <Shape> shapeSimpleObjectProperty;
    private SimpleObjectProperty <Coordinate2D> coordinate2DSimpleObjectProperty;
    private SimpleBooleanProperty startIsDisable;
    private SimpleBooleanProperty stopIsDisable;
    private SimpleMapProperty <Coordinate2D, GameFieldElement> mapProperty;
    private Stage primaryStage;
    private Timeline timeline;
    private AnchorPane anchorPane;
    private Game game;

    public TcrushController(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.testBegin = new SimpleStringProperty("TCrush-LevelDefinition:");
        this.LevelParser = new SimpleStringProperty("");
        this.startIsDisable = new SimpleBooleanProperty(false);
        this.stopIsDisable = new SimpleBooleanProperty(true);
        this.tCruschDefTest = new SimpleStringProperty("TCrush-LevelDefinition:");
        this.consoleText = new SimpleStringProperty("Level fehlschalgen! Kein Zug mehr möglich!"); // Level gelöst
        this.levelGewinn = new SimpleStringProperty("Level gelöst");
        KeyFrame keyFrame = new KeyFrame(Duration.INDEFINITE, this::handleLaden);
        this.timeline = new Timeline(keyFrame);
        this.timeline.setCycleCount(-1);
        game=new Game();        

    }
    
    public void handleLaden(ActionEvent e) {

        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("All files", "*.*"));
        fc.setTitle("Open Level");
        //fc.setInitialDirectory(new File(""));


        fc.setInitialDirectory(new DirectoryChooser().getInitialDirectory());
       // Stage stage = (Stage)anchorPane.getScene().getWindow();
        File file = fc.showOpenDialog(this.primaryStage);
        String buffer = "";
        try {

            if (file == null) {
                throw new IOException();
            }
            InputStream is = new FileInputStream(file);
            level = jpp.tcrush.parse.LevelParser.parseStreamToLevel(is);
            //System.out.println(level);            
            is.close();
            game.setLevel(level);
            game.init();
            
        } catch (Exception exception) {
            this.submitErrorMessage("falsche Datei ausgewaehlt: " + exception.getMessage());
            exception.printStackTrace();
        }

        this.tCruschDefTest.set(buffer);
    }

    public void handleSpeichern(ActionEvent e) {

    }
    
    
    
    public void handleStart(ActionEvent e) {
        this.timeline.play();
        this.stopIsDisable.set(false);
        this.startIsDisable.set(true);
    }

    public void handleStop(ActionEvent e) {
        this.timeline.pause();
        this.startIsDisable.set(false);
        this.stopIsDisable.set(true);
    }


    private void submitStatusMessage(String text) {
        this.consoleText.set(this.consoleText.get() + "\n" + text);
    }

    private void submitErrorMessage(String text) {
        this.levelGewinn.set(this.levelGewinn.get() + "\n" + text);
    }

    public void setContainer(GridPane gridPane) {
        this.gridPane=gridPane;
        game.setGameDashboard(gridPane);
    }

    public Game getGame() {
        return game;
    }


}
