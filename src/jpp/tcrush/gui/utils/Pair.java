/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpp.tcrush.gui.utils;

import java.io.Serializable;

/**
 *
 * @author ppitbull
 */
public class Pair<K extends Object, V extends Object> implements Serializable {
    private K first;
    private V second;

    public Pair(K first, V second) {
        this.first = first;
        this.second = second;
    }

    public K getFirst() {
        return first;
    }

    public V getSecond() {
        return second;
    }

    public void setFirst(K first) {
        this.first = first;
    }

    public void setSecond(V second) {
        this.second = second;
    }
    
    
    @Override
    public String toString()
    {
        return "Pair<> first: "+first+" second: "+second;
    }
    
    @Override
    public boolean equals(Object o) 
    {
        if (this == o) return true;
        if (!(o instanceof Pair)) return false;

        Pair pair = (Pair) o;
        return pair.getFirst()==first && pair.getSecond()==second;
    }
    
    
}
