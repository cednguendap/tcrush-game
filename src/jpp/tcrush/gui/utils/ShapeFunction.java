/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpp.tcrush.gui.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import jpp.tcrush.gamelogic.Shape;
import jpp.tcrush.gamelogic.utils.Coordinate2D;

/**
 *
 * @author ppitbull
 */
public class ShapeFunction {
    
    public static Coordinate2D getShapeCenter(Shape shape) 
    {
        int centX=0,centY=0;
        Pair<Integer,Integer> size=ShapeFunction.getSize(shape);
        centX=(size.getFirst()%2==0?size.getFirst()/2:size.getFirst()/2+1)-1;
        centY=(size.getSecond()%2==0?size.getSecond()/2:size.getSecond()/2+1)-1;
        return new Coordinate2D(centX,centY);
    }
    //retourne l'abscice ou l'ordonné la plus petit (négative) du shape
    public static int getNegativePoint(Shape shape)
    {
        int neg=0;
        for(Coordinate2D point:shape.getPoints())
        {
            if(point.getX()<neg) neg=point.getX();
            if(point.getY()<neg) neg=point.getY();
        }     
        return neg;
    }
    
    //retourne la taile du shape (nombre de point en abscisse et en ordonné) 
    public static Pair<Integer,Integer> getSize(Shape shape)
    {
        Map<Integer, Boolean> shapeInX=new LinkedHashMap <>();
        Map<Integer, Boolean> shapeInY=new LinkedHashMap<>();

        for(Coordinate2D point:shape.getPoints())
        {
            shapeInX.putIfAbsent(point.getX(), false);
            shapeInY.putIfAbsent(point.getY(), false);
        }      
        return new Pair<Integer,Integer>(shapeInX.size(),shapeInY.size());
    }
    
    public static boolean coordIsInShape(Shape shape, Coordinate2D coord)
    {
        for(Coordinate2D point:shape.getPoints())
        {
            if(point.equals(coord)) return true;
        }
        return false;
    }
    
    public static Coordinate2D getValidPosition(Coordinate2D center,Shape shape)
    {
        Coordinate2D valid=new Coordinate2D(center.getX(),center.getY());
        Collection<Coordinate2D> collection=new ArrayList<>();
        for(Coordinate2D coord:shape.getPoints())
        {
            collection.add(new Coordinate2D(coord.getX(),coord.getY()));
        }
        Shape nshape=new Shape(collection,shape.getAmount());
        
        boolean foundValid=ShapeFunction.coordIsInShape(nshape, valid);    
        System.out.println("Shape "+nshape+" valid "+valid);
        System.out.println("Found valide center "+foundValid);
        if(foundValid) return valid;
        
        int j=valid.getY();
        while(foundValid==false && j<ShapeFunction.getSize(nshape).getSecond())
        {
            valid.setY(j);
            if(ShapeFunction.coordIsInShape(nshape, valid)) foundValid=true;
            j++;
        }
        System.out.println("Found valide vers le bas "+nshape+" valid "+valid);
        if(foundValid) return valid;
        
        
        j=valid.getY();
        while(foundValid==false && j<ShapeFunction.getSize(nshape).getSecond())
        {
            valid.setY(j);
            if(ShapeFunction.coordIsInShape(nshape, valid)) foundValid=true;
            j--;
        }
        return valid;
    }
    
}
