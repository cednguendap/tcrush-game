package jpp.tcrush.parse;

import jpp.tcrush.gamelogic.Level;
import jpp.tcrush.gamelogic.Shape;
import jpp.tcrush.gamelogic.field.GameFieldElement;
import jpp.tcrush.gamelogic.utils.Coordinate2D;

import java.io.*;
import java.util.*;

public class LevelParser {

    private static int count;

    public static Level parseStreamToLevel(InputStream inputStream) {

        String input;
        Map <Coordinate2D, GameFieldElement> mapLevel = new LinkedHashMap <>();
        Shape shapeLevel;
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        Collection <Shape> shapes = new LinkedList <>();
        BufferedReader reader = new BufferedReader(inputStreamReader);
        Scanner scanner = new Scanner(inputStream);
        count = 0;
        String[] tab = {"",""};
        Boolean firstEntry = true;

        while (scanner.hasNextLine()) {
            input = scanner.nextLine();
            if (firstEntry && !(input.startsWith("TCrush-LevelDefinition:") || input.startsWith("Shapes:"))){
                throw new InputMismatchException();
            }

            if (firstEntry && input.startsWith("TCrush-LevelDefinition:")) {
                count = 0;
                firstEntry = false;
                continue;
            } else if (firstEntry && input.startsWith("Shapes:")) {
                count = 1;
                firstEntry = false;
                continue;
            } else if (input.isEmpty()) {
                firstEntry = true;
                continue;
            }
            tab[count] += input + "\n";

        }

        System.out.println("Tab\n" + Arrays.toString(tab));
        mapLevel = ParserUtils.parseStringToFieldMap(tab[0]);
        String[] temp = tab[1].split("\n");
        for (int i = 0; i < temp.length; i++) {
            shapeLevel = ParserUtils.parseStringToShape(temp[i]);
            shapes.add(shapeLevel);
        }

        return new Level(mapLevel, shapes);
    }

    public static void writeLevelToStream(Level level, OutputStream outputStream) {

        PrintWriter printWriter = new PrintWriter(outputStream, true);

        StringBuilder output = new StringBuilder(level.toString());

        printWriter.print(output.toString());
        printWriter.flush();

        System.out.print("WriteTest \n" + output);

    }

    public static void main(String[] args) {

        StringBuilder stringBuilder = new StringBuilder();

        String test = "TCrush-LevelDefinition:\ngb\n+r\n+r\nbr\n\nShapes:\n1:point\n1:column\n1:sRow";
        String[] stringstest = test.split("\n\n");

        System.out.println("length\n" + stringstest.length);




    }
}
