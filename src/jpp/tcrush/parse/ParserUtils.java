package jpp.tcrush.parse;

import jpp.tcrush.gamelogic.Shape;
import jpp.tcrush.gamelogic.field.*;
import jpp.tcrush.gamelogic.utils.Coordinate2D;

import java.util.*;

public class ParserUtils {

    public static Coordinate2D parseStringToCoordinate(String s) {


        if (s.charAt(0) != '(' || s.charAt(s.length() - 1) != ')' || s.length() < 5 || s.contains(".")) {
            throw new InputMismatchException();
        }

        s = s.replace("(", "");
        s = s.replace(")", "");

        String[] stringsArray = s.split(",");
        if (stringsArray.length != 2) {
            throw new InputMismatchException();
        }

        double a = Double.parseDouble(stringsArray[0]);
        double b = Double.parseDouble(stringsArray[1]);

        return new Coordinate2D((int) a, (int) b);
    }

    public static Shape parseStringToShape(String s) {

        if (!s.contains(":")) {
            throw new InputMismatchException();
        }

        String[] stringsArrays = s.split(":");
        int amount = 0;
        try {
            amount = Integer.parseInt(stringsArrays[0]);
            if (amount <= 0) {
                throw new InputMismatchException();
            }
        } catch (Exception e) {
            throw new InputMismatchException();
        }

        String giveShapeName = stringsArrays[1];

        switch (giveShapeName) {
            case "point":
                return Shape.getPointShape(amount);
            case "sRow":
                return Shape.getsRowShape(amount);
            case "sColumn":
                return Shape.getsColumnShape(amount);
            case "row":
                return Shape.getRowShape(amount);
            case "column":
                return Shape.getColumnShape(amount);
            case "upT":
                return Shape.getUpTShape(amount);
            case "downT":
                return Shape.getDownTShape(amount);
            case "rightT":
                return Shape.getRightTShape(amount);
            case "leftT":
                return Shape.getLeftTShape(amount);

        }
        if (giveShapeName.charAt(giveShapeName.length() - 1) != ';') {
            throw new InputMismatchException();
        }
        Collection <Coordinate2D> coordinate2DCollection = new ArrayList <>();
        try {
            String[] coordonateShapes = stringsArrays[1].split(";");

            for (String coordinateShape : coordonateShapes) {
                if (!coordinateShape.equals("")) {
                    coordinate2DCollection.add(parseStringToCoordinate(coordinateShape));
                }
            }
        } catch (Exception e) {
            throw new InputMismatchException();
        }

        return new Shape(coordinate2DCollection, amount);

    }

    public static Map <Coordinate2D, GameFieldElement> parseStringToFieldMap(String s) {
        //System.out.println("String \n" + s);

        String[] stringsLine = s.split("\n");
        if (stringsLine.length <= 1) {
            throw new InputMismatchException();
        }
        for (String line : stringsLine) {
            if (stringsLine[0].length() != line.length() || line.length() <= 1) {
                throw new InputMismatchException();
            }
        }

        for (int i = 0; i < stringsLine[0].length(); i++) {

            if (stringsLine[0].charAt(i) == '+' || stringsLine[stringsLine.length - 1].charAt(i) == '+') {
                throw new InputMismatchException();
            }
        }
        Map <Coordinate2D, GameFieldElement> elementMap = new LinkedHashMap <>();
        for (int i = 0; i < stringsLine.length; i++) {
            for (int j = 0; j < stringsLine[i].length(); j++) {

                switch (stringsLine[i].charAt(j)) {
                    case 'b':
                        elementMap.put(new Coordinate2D(j, i), new Cell(new GameFieldItem(GameFieldItemType.BLUE), new Coordinate2D(j, i)));
                        break;
                    case 'g':
                        elementMap.put(new Coordinate2D(j, i), new Cell(new GameFieldItem(GameFieldItemType.GREEN), new Coordinate2D(j, i)));
                        break;
                    case 'r':
                        elementMap.put(new Coordinate2D(j, i), new Cell(new GameFieldItem(GameFieldItemType.RED), new Coordinate2D(j, i)));
                        break;
                    case 'y':
                        elementMap.put(new Coordinate2D(j, i), new Cell(new GameFieldItem(GameFieldItemType.YELLOW), new Coordinate2D(j, i)));
                        break;
                    case 'p':
                        elementMap.put(new Coordinate2D(j, i), new Cell(new GameFieldItem(GameFieldItemType.PURPLE), new Coordinate2D(j, i)));
                        break;
                    case 'B':
                        elementMap.put(new Coordinate2D(j, i), new Cell(new GameFieldItem(GameFieldItemType.BLACK), new Coordinate2D(j, i)));
                        break;
                    case 'o':
                        elementMap.put(new Coordinate2D(j, i), new Cell(new GameFieldItem(GameFieldItemType.ORANGE), new Coordinate2D(j, i)));
                        break;
                    case '#':
                        elementMap.put(new Coordinate2D(j, i), new Block(new Coordinate2D(j, i)));
                        break;
                    case '+':
                        elementMap.put(new Coordinate2D(j, i), new Fallthrough(new Coordinate2D(j, i)));
                        break;
                    case 'n':
                        elementMap.put(new Coordinate2D(j, i), new Cell(null, new Coordinate2D(j, i)));
                        break;
                    default:
                        throw new InputMismatchException();
                }
            }
        }


        for (int i = 0; i < stringsLine.length; i++) {
            for (int j = 0; j < stringsLine[0].length(); j++) {
                GameFieldElement gameFieldElement = elementMap.get(new Coordinate2D(j, i));

                if (gameFieldElement.getType() != GameFieldElementType.FALLTHROUGH && gameFieldElement.getType() != GameFieldElementType.BLOCK) {
                    for (int k = i + 1; k < stringsLine.length && elementMap.get(new Coordinate2D(j, k)).getType() != GameFieldElementType.BLOCK; k++) {
                        if (elementMap.get(new Coordinate2D(j, k)).getType() != GameFieldElementType.FALLTHROUGH) {
                            gameFieldElement.setSuccessor(elementMap.get(new Coordinate2D(j, k)));
                            elementMap.get(new Coordinate2D(j, k)).setPredecessor(gameFieldElement);
                            break;
                        }

                    }

                }
            }
        }
        return elementMap;
    }

    public static void main(String[] args) {

        String test = "bgrypBo\n" + "nnnnnnn\n" + "+++++++\n" + "+++nnnn\n" + "nnnnnnn\n" + "#######";
        Map <Coordinate2D, GameFieldElement> testMap = parseStringToFieldMap(test);
        System.out.println(testMap);

    }
}
